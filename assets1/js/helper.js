// JavaScript Document

// The function returns true if a valid date, false if not.
// ******************************************************************
function isDate(dateStr) {

    var datePat = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
    var matchArray = dateStr.match(datePat); // is the format ok?

    if (matchArray == null) {
        //alert("Please enter date as either mm/dd/yyyy or mm-dd-yyyy.");
        return false;
    }

    month = matchArray[1]; // p@rse date into variables
    day = matchArray[3];
    year = matchArray[5];

    if (month < 1 || month > 12) { // check month range
        //alert("Month must be between 1 and 12.");
        return false;
    }

    if (day < 1 || day > 31) {
        //alert("Day must be between 1 and 31.");
        return false;
    }

    if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 31) {
        //alert("Month " + month + " doesn`t have 31 days!")
        return false;
    }

    if (month == 2) { // check for february 29th
        var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
        if (day > 29 || (day == 29 && !isleap)) {
            //alert("February " + year + " doesn`t have " + day + " days!");
            return false;
        }
    }
    return true; // date is valid
}

function isPasswordValid(val){
	//Mininum 8 charatcer, Alaphanumeric, 1 Uppercase, 1 lowercase
	var validated =  true;
	if(val.length < 8)
		validated = false;
	if(!/[a-z]/.test(val))
		validated = false;
	if(!/[A-Z]/.test(val))
		validated = false;
	return (validated);
}

function isPasswordValidPlusSpecial(val){
	//Mininum 8 charatcer, Alaphanumeric, 1 Uppercase, 1 lowercase, 1 special
	var validated =  true;
	if(val.length < 8)
		validated = false;
	if(!/\d/.test(val))
		validated = false;
	if(!/[!@#$%^&*()_=\[\]{};':"\\|,.<>\/?+-]/.test(val)){
		validated = false;
	}
	if(!/[a-z]/.test(val))
		validated = false;
	if(!/[A-Z]/.test(val))
		validated = false;
	if(/![0-9a-zA-Z]/.test(val)){
		validated = false;
	}
	return (validated);
}