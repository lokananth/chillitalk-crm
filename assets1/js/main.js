/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


// Save Topup Selection Values
function saveTopupPackSelection(url, isCardSaved) {
//    alert($("#topup_selection").val());
//    $("#topup_error").html("");
//    if ($("input[name='topup']:checked").val() === undefined && $("input[name='data_bundle']:checked").val() === undefined && $("input[name='smart_student_bundle']:checked").val() === undefined && $("input[name='packet_saver_bundle']:checked").val() === undefined) {
//        $("#topup_error").html("Please select any top up packs");
//    }
//    else {
       showLoading();
        $.ajax({
            type: "POST",
            url: url + "topup/save_topup_selection",
            data: {
                topupPackId: $("#topup_selection_id").val(),
                topupPack: $("#topup_selection").val(),
                topupPackProductId: $("#topup_selection_product_id").val(),
                dataBundleId: $('#dataBundle').val() === "1" ? $("#bundle1_selection_id").val() : $("#hdn-bnd").val(),
                dataBundle: $('#dataBundle').val() === "1" ? $("#bundle1_selection_name").val() : $("#hdn-bnd").val(),
                dataBundleAmount: $('#dataBundle').val() === "1" ? $("#bundle1_selection_price").val() : $("#hdn-bnd").val(),
                smartStudentAllBundleId: $('#smart-all').val() === "1" ? $("#bundle2_selection_id").val() : $("#hdn-bnd").val(),
                smartStudentAllBundle: $('#smart-all').val() === "1" ?  $("#bundle2_selection_name").val() : $("#hdn-bnd").val(),
                smartStudentAllBundleAmount: $('#smart-all').val() === "1" ?  $("#bundle2_selection_price").val() : $("#hdn-bnd").val(),
                smartStudentIndiaBundleId: $('#smart-ind').val() === "1" ? $("#bundle3_selection_id").val() : $("#hdn-bnd").val(),
                smartStudentIndiaBundle: $('#smart-ind').val() === "1" ? $("#bundle3_selection_name").val() : $("#hdn-bnd").val(),
                smartStudentIndiaBundleAmount: $('#smart-ind').val() === "1" ? $("#bundle3_selection_price").val() : $("#hdn-bnd").val(),
                smartStudentChinaBundleId: $('#smart-chin').val() === "1" ? $("#bundle4_selection_id").val() : $("#hdn-bnd").val(),
                smartStudentChinaBundle: $('#smart-chin').val() === "1" ? $("#bundle4_selection_name").val() : $("#hdn-bnd").val(),
                smartStudentChinaBundleAmount: $('#smart-chin').val() === "1" ? $("#bundle4_selection_price").val() : $("#hdn-bnd").val(),
                packetSaverBangBundleId: $('#pocket-bang').val() === "1" ? $("#bundle5_selection_id").val() : $("#hdn-bnd").val(),
                packetSaverBangBundle: $('#pocket-bang').val() === "1" ? $("#bundle5_selection_name").val() : $("#hdn-bnd").val(),
                packetSaverBangBundleAmount: $('#pocket-bang').val() === "1" ? $("#bundle5_selection_price").val() : $("#hdn-bnd").val(),
                packetSaverPakBundleId: $('#pocket-pak').val() === "1" ? $("#bundle6_selection_id").val() : $("#hdn-bnd").val(),
                packetSaverPakBundle: $('#pocket-pak').val() === "1" ? $("#bundle6_selection_name").val() : $("#hdn-bnd").val(),
                packetSaverPakBundleAmount: $('#pocket-pak').val() === "1" ? $("#bundle6_selection_price").val() : $("#hdn-bnd").val(),
                packetSaverSlBundleId: $('#pocket-sl').val() === "1" ? $("#bundle7_selection_id").val() : $("#hdn-bnd").val(),
                packetSaverSlBundle: $('#pocket-sl').val() === "1" ? $("#bundle7_selection_name").val() : $("#hdn-bnd").val(),
                packetSaverSlBundleAmount: $('#pocket-sl').val() === "1" ? $("#bundle7_selection_price").val() : $("#hdn-bnd").val(),
                autoTopup: $("input[name='auto_topup']:checked").val() === "on" ? "true" : "false"
            },
            success: function (response) {
                hideLoading();
                if (response) {
                    //if (isCardSaved === "0") {
                        window.location.replace(url + "topup/card_details/new_card");
                  //  }
//                    else {
//                        window.location.replace(url + "topup/card_details/saved_card");
//                    }
                }
            }
        });
//    }
    return false;
}






function setLanguage(langName, langCode) {
    showLoading();
    $.ajax({
        type: "POST",
        url: $('#site_url').val() + "base_controller/set_language",
        data: {
            langName: langName,
            langCode: langCode
        },
        success: function (response) {
            hideLoading();
            if (response) {
                window.location.reload();
            }
        }
    });
    return false;
}

$(document).ready(function() {
    
    $.ajaxSetup({
        data: {
            csrf_test_name: $.cookie('csrf_cookie_name')
        }
    });
    
    // User Phone Number Dropdown selection
    $('#mobile-list li a').on('click', function () {
        $('#mobile').html($(this).html() + ' ');
    });
    
    // Multiligual dropdown setup
    $('#flag-list li a').on('click', function () {
        $('#flag').html('<img src="'+ $(this).children('img').attr('src') +'" /> ');
        setLanguage($(this).attr("alt"), $(this).attr("id"));
        return false;
    });
    
    $('#flag').html('<img src="'+ $('#'+$('#selected_lang').val()).children('img').attr('src') +'" /> <span class="caret"></span>');
    
    if($("#topup_selection_id").val() != "") {
    $("#"+$("#topup_selection_id").val()).addClass('active');
    }
    
    if($("#data_bundle_selection_id").val() != "") {
    $("#"+$("#data_bundle_selection_id").val()).addClass('active');
    }
    
    if($("#smart_students_bundle_selection_id").val() != "") {
    $("#"+$("#smart_students_bundle_selection_id").val()).addClass('active');
    }
    
    if($("#packet_savers_bundle_selection_id").val() != "") {
    $("#"+$("#packet_savers_bundle_selection_id").val()).addClass('active');
    }
    
    $('li.topup-packs').click(function () {
        var packId = $(this).attr('id');
        $("#topup_selection_id").val(packId);
        $("#topup_selection").val($('#hdn-' + packId).val());
        $("#topup_selection_product_id").val($('#hdn-product-' + packId).val());
        $("#flag_amount").html($("#topup_selection").val());
    });

//    $('li.data-bundle').click(function () {
//        var bundleId = $(this).attr('id');
//        $("#data_bundle_selection_id").val(bundleId);
//        $("#data_bundle_selection").val($('#hdn-' + bundleId).val());
//        $("#data_bundle_selection_amount").val($('#hdn-amount-' + bundleId).val());
//    });
//
//    $('li.smart-bundle').click(function () {
//        var bundleId = $(this).attr('id');
//        $("#smart_students_bundle_selection_id").val(bundleId);
//        $("#smart_students_bundle_selection").val($("#hdn-" + bundleId).val());
//        $("#smart_students_bundle_selection_amount").val($("#hdn-amount-" + bundleId).val());
//    });
//
//    $('li.packet-saver-bundle').click(function () {
//        var bundleId = $(this).attr('id');
//        $("#packet_savers_bundle_selection_id").val(bundleId);
//        $("#packet_savers_bundle_selection").val($("#hdn-" + bundleId).val());
//        $("#packet_savers_bundle_selection_amount").val($("#hdn-amount-" + bundleId).val());
//    });
    
    // Set Auto Topup Selection Value
    if ($('#hdn_auto_topup').val() == 0) {
        $("[name='auto_topup']").bootstrapSwitch('state', false);
    }
    else {
        $("[name='auto_topup']").bootstrapSwitch('state', true);
    }
    
    $('#btn_find').click(function() {
        if($('#post_code').val() != "") {
        $('#fr_address_info').fadeIn();
        $(this).text("Change");
    }
    else {
        alert("Please enter the post code");
    }
        return false;
    });
    
//    // Set Topup Pack Selection Value
//    if($('#topup_selection').val() != "") {
//    $("input[name=topup][value=" + $('#topup_selection').val() + "]").attr('checked', 'checked');
//    }
//    
//    // Set Data Bundle Selection Value
//    if($('#data_bundle_selection').val() != "") {
//    $("input[name=data_bundle][value=" + $('#data_bundle_selection').val() + "]").attr('checked', 'checked');
//    }
//    
//    // Set Smart Students Bundle Selection Value
//    if($('#smart_students_bundle_selection').val() != "") {
//    $("input[name=smart_student_bundle][value='" + $('#smart_students_bundle_selection').val() + "']").attr('checked', 'checked');
//    }
//    
//    // Set Packet Savers Bundle Selection Value
//    if($('#packet_savers_bundle_selection').val() != "") {
//    $("input[name=packet_saver_bundle][value='" + $('#packet_savers_bundle_selection').val() + "']").attr('checked', 'checked');
//    }

    // Notification Bootstrap Switch Setup
    $("[name='notify_email']").bootstrapSwitch();
    $("[name='notify_sms']").bootstrapSwitch();
    $("[name='notify_phone']").bootstrapSwitch();
    
    if($("input[name='topup']:checked").val() === undefined) {
        $("#auto_topup_row").fadeOut();
    }
    
    // Topup packs selection
    $("#topup_packs input:checkbox").change(function () {
        if (this.checked) {
            
            var checkname = $(this).attr("name");
            $("input:checkbox[name='" + checkname + "']").not(this).removeAttr("checked");
            $("#auto_topup_row").fadeIn();
            $("#topup_error").html("");
//            $("[name='auto_topup']").bootstrapSwitch('toggleReadonly');
//            $("[name='auto_topup']").bootstrapSwitch('state', true);
//            $("[name='auto_topup']").bootstrapSwitch('toggleEnabled');
//            
        }
        else {
            $("#auto_topup_row").fadeOut();
//           $("[name='auto_topup']").bootstrapSwitch('state', false);
//           $("[name='auto_topup']").bootstrapSwitch('toggleReadonly');
        }
    });
    
    // Smart Students Bundle Selection
    $("#smart_students_bundle input:checkbox").change(function () {
        if (this.checked) {
            var checkname = $(this).attr("name");
            $("input:checkbox[name='" + checkname + "']").not(this).removeAttr("checked");
            $("#topup_error").html("");
        }
    });
    
    // Packet Savers Bundle Selection
    $("#packet_savers_bundle input:checkbox").change(function () {
        if (this.checked) {
            var checkname = $(this).attr("name");
            $("input:checkbox[name='" + checkname + "']").not(this).removeAttr("checked");
            $("#topup_error").html("");
        }
    });

	$('.number-only').keypress(function(event){
		var key = event.charCode ? event.charCode : event.keyCode;
		if (key == 8) {
			return true;
		}
		if (key >= 48 && key <= 57) {
			return true;
		} else {
			return false;
		}
		return true;
   });        

});

function showLoading(){
	$('.load-bg').css('display', 'block');
}

function hideLoading(){
	$('.load-bg').css('display', 'none');
}

function resetForm(formid) {
 $(':input','#'+formid) .not(':button, :submit, :reset, :hidden') .val('') .removeAttr('checked') .removeAttr('selected');
 }
 
function logOut(url, brand) {
    $('.load-bg').css('display', 'block');
    $.ajax({
        type: "POST",
        url: $('#site_url').val() + "user/logout",
        data: {
            logout: true,
        },
        success: function (response) {
            $('.load-bg').css('display', 'none');
            if (response) {
                if (response) {
                    if(brand == "2") {
                    window.location.replace(url + "/myaccount/logout");
                }
                else {
                    window.location.replace($('#site_url').val() + "user/login");
                }
                }
            }
        }
    });
    return false;
}


function clearSession() {
    $.ajax({
        type: "POST",
        url: $('#site_url').val() + "topup/clear_card_info",
        data: {
            isClear: true
        },
        success: function (response) {

        }
    });
}




//window.onload = init;
//var interval;
//function init()
//{
//    interval = setInterval(trackLogin, 5000);
//}
//function trackLogin()
//{
//    var xmlReq = false;
//    try {
//        xmlReq = new ActiveXObject("Msxml2.XMLHTTP");
//    } catch (e) {
//        try {
//            xmlReq = new ActiveXObject("Microsoft.XMLHTTP");
//        } catch (e2) {
//            xmlReq = false;
//        }
//    }
//    if (!xmlReq && typeof XMLHttpRequest != 'undefined') {
//        xmlReq = new XMLHttpRequest();
//    }
//
//    xmlReq.open('get', $('#site_url').val() + 'user/chek_session', true);
//    xmlReq.setRequestHeader("Connection", "close");
//    xmlReq.send(null);
//    xmlReq.onreadystatechange = function () {
//        if (xmlReq.readyState == 4 && xmlReq.status == 200) {
//            if (xmlReq.responseText == 1)
//            {
//                clearInterval(interval);
//                alert('You have been logged out.You will now be redirected to home page.');
//                document.location.href = "index.html";
//            }
//        }
//    }
//}
