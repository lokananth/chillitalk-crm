<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This class object used to manage the user Session
 *
 * @author Shanmuganathan Velusamy
 */

class user extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('UTC');
        $this->load->helper(array('url', 'form', 'portal', 'api'));
        $this->load->model('helper_model');

        if (!isset($_SESSION)) {
            session_start();
        }
    }
    
    /**
     * Index function
     */
    public function index() {
        $this->login();
    }
    
    /**
     * User Login
     */
    public function login() {
        $data['main_content'] = 'user/login';
        $this->load->view('login_template', $data);
    }

    /**
     * LogOut
     */
    public function logout() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST'):

        endif;
    }

}
