<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dashboard
 *
 * @author Shanmuganathan Velusamy
 */
class dashboard extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
    }
    
    public function index() {
        $data['main_content'] = 'dashboard/dashboard';
        $this->load->view('template', $data);
    }

}
