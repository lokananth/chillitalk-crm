<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This class object is the base class that every library and helper files are loadad in.
 *
 * @author Shanmuganathan Velusamy
 */
class base_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('UTC');
        if (!isset($_SESSION)) {
            session_start();
        }
        ob_start();

        $this->load->helper(array('url', 'form', 'language'));
        $this->load->library('session');
        $this->load->helper('email');
        $this->load->model('helper_model');
    }

}
