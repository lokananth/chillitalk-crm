<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of new_helper
 *
 * @author s.velusamy
 */
defined('BASEPATH') OR exit('No direct script access allowed');

function get_timeline_data() {
    return json_decode(file_get_contents(base_url("assets/json/timeline.json")), true);
}

function get_llom_timeline_data() {
    return json_decode(file_get_contents(base_url("assets/json/llom_timeline.json")), true);
}

function get_llom_reactivation_timeline_data() {
    return json_decode(file_get_contents(base_url("assets/json/llom_reactivation_time_line.json")), true);
}

function get_llom_pay_by_balance_timeline_data() {
    return json_decode(file_get_contents(base_url("assets/json/llom_pay_by_balance_timeline.json")), true);
}

function get_country_saver_timeline_data() {
    return json_decode(file_get_contents(base_url("assets/json/country_saver_details.json")), true);
}

function get_country_saver_balance_timeline_data() {
    return json_decode(file_get_contents(base_url("assets/json/country_saver_pay_balance.json")), true);
}

function get_country_saver_reactivate_timeline_data() {
    return json_decode(file_get_contents(base_url("assets/json/reactivate_country_saver_details.json")), true);
}

function get_card_details($cardStatus) {
    $CI = & get_instance();
    if ($cardStatus == "new_card"):
        $card_details = (object) $CI->session->userdata('arrPaymentDetails');
    elseif ($cardStatus == "saved_card"):
        $card_details = (object) get_saved_card_details();
    else:
        show_404();
    endif;
    return $card_details;
}

function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if (getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if (getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if (getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if (getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if (getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    
    if($ipaddress == "::1"):
        $ipaddress = "192.168.13.75";
    endif;

    return $ipaddress;
}

/**
 * Get Saved Card Details
 * @return type
 */
function get_saved_card_details() {
    $CI = & get_instance();
//    $param = array(
//        "msisdn" => $CI->helper_model->getLoggedPhoneNumber(),
//        "sitecode" => $CI->helper_model->getSiteCode()
//    );
    $param = array(
        "mobileNo" => $CI->helper_model->getLoggedPhoneNumber()
    );
    $response = json_decode(apiPost(config_item('myaccount_api_endpoint')."GetCCUpdates", $param), TRUE);
    return $response;
}

/**
 * Get Personal Details
 */
function get_personal_details() {
    $CI = & get_instance();

    $data["mobileno"] = $CI->helper_model->getLoggedPhoneNumber();
    $data["subscriberid"] = $CI->helper_model->getSubscriberId();
    $data["sitecode"] = $CI->helper_model->getSiteCode();
    $data["brand"] = $CI->helper_model->getBrand();

    $response = json_decode(apiPost(config_item('myaccount_api_endpoint')."web_get_personal_details_myaccount", $data), TRUE);
    return $response;
}

/** 
 * Get Balance Details
 * @return type
 */
function get_user_details() {
    $CI = & get_instance();
    $userSession = (object) $CI->session->userdata('userSession');
    $param = array(
        'msisdn' => $userSession->phoneNumber,
        'passwd' => $userSession->password,
        'sitecode' => $userSession->siteCode,
        'brand' => $userSession->brand
    );

    $response = json_decode(apiPost(config_item('personal_login_myaccount'), $param), TRUE);
    return $response;
}

 /**
 * Get Card Status
 * @return type
 */
function get_card_status() {
    $CI = & get_instance();
    $userSession = (object) $CI->session->userdata('userSession');
    $data = array(
        "Mobileno" => $userSession->phoneNumber,
        "Sitecode" => $userSession->siteCode
    );
    $response = json_decode(apiPost(config_item('myaccount_api_endpoint') . "GetCcardStatus", $data), TRUE);
    return $response;
}


/**
 * Save Credit Card Details
 */
function save_card_details($cardDetails) {
    $CI = & get_instance();
    $userSession = (object) $CI->session->userdata('userSession');
    
    $param = array(
        "mobileNo" => $userSession->phoneNumber,
        "sitecode" => $userSession->siteCode,
        "ccNumber" => substr($cardDetails->cardNumber, -6),
        "houseNo" => $cardDetails->houseNumber,
        "streetName" => $cardDetails->streetName,
        "city" => $cardDetails->city,
        "country" => $cardDetails->country,
        "createdby" => $userSession->productCode,
        "postcode" => $cardDetails->postCode
    );
    
    apiPost(config_item('myaccount_api_endpoint') . "InsertCCUpdates", $param);
}

/**
 * Send SMS
 * @param type $message
 */
function send_sms($phoneNumber, $message) {
    
    //$phoneNumber = "919003017681";
    
    $param = array(
        "mobileno" => $phoneNumber,
        "message" => $message,
    );

    apiPost(config_item('myaccount_api_endpoint') . "SendOnlySMS", $param);
}

/**
 * Get Order ID
 * @param type $refCode
 */
function get_order_id($refCode) {
    if ($refCode != NULL):
        $arrRef = explode("-", $refCode);
        return $arrRef[3];
    else:
        return "0";
    endif;
}

/**
 * Clear All Payment Session
 */
function clear_payment_session() {
    unset($_SESSION['paymentSession']);
    unset($_SESSION['creditcard_payment_flag']);
}

/**
 * Clear All Card Details Session
 */
function clear_card_details_session() {
    $CI = & get_instance(); 
    $CI->session->unset_userdata('arrPaymentDetails');
    ob_clean();
}

/**
 * Encrypt URL
 * @param type $string
 * @return type
 */
function encrypt_url($string) {
  $key = "SHAN_979805"; //key to encrypt and decrypts.
  $result = '';
  $test = "";
   for($i=0; $i<strlen($string); $i++) {
     $char = substr($string, $i, 1);
     $keychar = substr($key, ($i % strlen($key))-1, 1);
     $char = chr(ord($char)+ord($keychar));

     $test[$char]= ord($char)+ord($keychar);
     $result.=$char;
   }
   $str = base64_encode($result);
   $strFilter = base64url_encode($str);
   return urlencode($strFilter);
}

/**
 * Decrypt URL 
 * @param type $string
 * @return type
 */
function decrypt_url($string) {
    $key = "SHAN_979805"; //key to encrypt and decrypts.
    $result = '';
    $str = base64url_decode($string);
    $string = base64_decode(urldecode($str));
   for($i=0; $i<strlen($string); $i++) {
     $char = substr($string, $i, 1);
     $keychar = substr($key, ($i % strlen($key))-1, 1);
     $char = chr(ord($char)-ord($keychar));
     $result.=$char;
   }
   return $result;
}

function base64url_encode($s) {
    return str_replace(array('+', '/'), array('-', '_'), base64_encode($s));
}

function base64url_decode($s) {
    return base64_decode(str_replace(array('-', '_'), array('+', '/'), $s));
}

/**
 * Get Account List
 * @return type
 */
function get_account_list() {
    $CI = & get_instance();
    $userSession = $CI->session->userdata('userSession');

    $data = array(
        'msisdn' => $userSession['phoneNumber'],
        'sitecode' => $userSession['siteCode'],
        'brand' => $userSession['brand']
    );

    $response = json_decode(apiPost(config_item('get_manage_account'), $data), TRUE);

    if (isset($response['errcode'])) {
        if ($response['errcode'] == -1) {
            return Array();
        }
        if (isset($response['errmsg'])) {
            if ($response['errmsg'] == 'No data found') {
                return Array();
            }
        }
    } else {
        return $response;
    }
}

/*
 * Get Payment History Details
 */

function get_payment_history_details($month = NULL) {
    $CI = & get_instance();
    $count = 5;
    $stardDate = '';
    $endDate = '';
    if ($month != NULL) {
        $sdate = $CI->helper_model->getMonthFirstDate($month);
        $edate = $CI->helper_model->getMonthEndDate($month);
        $stardDate = strtoupper(date('d/M/Y', strtotime($sdate)));
        $endDate = strtoupper(date('d/M/Y', strtotime($edate)));
        $count = -1;
    }

    $userSession = $CI->session->userdata('userSession');
    $params = array(
        "mobileno" => $userSession['phoneNumber'],
        "startdate" => $stardDate,
        "enddate" => $endDate,
        "sitecode" => $userSession['siteCode'],
        "brand" => $userSession['brand'],
        "count" => $count
    );

    $response = json_decode(apiPost(config_item('get_payment_history'), $params), TRUE);

    if (isset($response['errcode'])) {
        if ($response['errcode'] == -1) {
            return Array();
        }
    }

    return $response;
}


/*
 * Get Call History Details
 */

function get_call_history_details($type = 0, $month = NULL) {
    $CI = & get_instance();
    $count = 5;
    $stardDate = '';
    $endDate = '';
    if ($month != NULL) {
        $sdate = $CI->helper_model->getMonthFirstDate($month);
        $edate = $CI->helper_model->getMonthEndDate($month);
        $stardDate = strtoupper(date('d/M/Y', strtotime($sdate)));
        $endDate = strtoupper(date('d/M/Y', strtotime($edate)));
        $count = -1;
    }

    $userSession = $CI->session->userdata('userSession');
    $params = array(
        "Msisdn" => $userSession['phoneNumber'],
        "DateFrom" => $stardDate,
        "DateTo" => $endDate,
        "SiteCode" => $userSession['siteCode'],
        "count" => $count,
        "Type" => $type
    );


    $response = json_decode(apiPost(config_item('get_call_history'), $params), TRUE);

    if (isset($response['errcode'])) {
        if ($response['errcode'] == -1) {
            return Array();
        }
    }

    return $response;
}

function curPageURL() {
    $pageURL = 'http';
    if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

function domainName($url) {
    $arrUrl = parse_url($url);
    $domain = $arrUrl['host'];
    if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
        return $regs['domain'];
    }
}

//function checkIfTimedOut() {
//    $current = time(); // take the current time
//    $diff = $current - $_SESSION['loggedAt'];
//    if ($diff > $_SESSION['timeOut']) {
//        return true;
//    } else {
//        return false;
//    }
//}
