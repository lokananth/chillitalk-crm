<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of api_helper
 *
 * @author Shanmuganathan Velusamy
 */

defined('BASEPATH') OR exit('No direct script access allowed');


function apiGet($apiUrl, $isAuth = true) {
    $headers = array(
        'Authorization: Basic OGFrMzdnSlVZcTJoUno6MGRpdWQ3NjVqZzk0YnNpODRqZmdqMHczamZoNzgyMmo=',
        'Host: webapi.vectone.com'
    );
	if (!$isAuth){
		$headers = array();
	}

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, 'geoPlugin PHP Class v1.0');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	if ($isAuth){
    	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	}
    $response = curl_exec($ch);
    if ($response === FALSE) {
        $response = htmlspecialchars(curl_errno($ch));
    }
    curl_close($ch);
    return $response;
}

function apiPost($apiUrl, $data, $isAuth = true) {

  // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen(json_encode($data)),
        'Authorization: Basic OGFrMzdnSlVZcTJoUno6MGRpdWQ3NjVqZzk0YnNpODRqZmdqMHczamZoNzgyMmo=',
        'Host: webapi.vectone.com'
    ));
   // execute the request
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

function apiPatch($apiUrl, $data, $isAuth = true) {

// set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen(json_encode($data)),
        'X-HTTP-Method-Override: PATCH'
    ));
	
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen(json_encode($data)),
        'X-HTTP-Method-Override: PATCH'
    ));
	
    $output = curl_exec($ch);
	 //$info = curl_getinfo($ch);
	 //print_r($info);
     curl_close($ch);
	 return true;
}

function apiPost1($apiUrl, $data, $isAuth = true) {

  // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen(json_encode($data))
    ));
   // execute the request
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

function apiPost2($apiUrl, $data, $isAuth = true) {

  // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen(json_encode($data)),
        'Authorization: Basic OGFrMzdnSlVZcTJoUno6MGRpdWQ3NjVqZzk0YnNpODRqZmdqMHczamZoNzgyMmo=',
        'Host: 192.168.2.102:9030'
    ));
   // execute the request
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

function paysafeApiPostOld($apiUrl, $data = Array(), $isAuth = true) {

	$header[0] = 'Authorization: mundiovectone y7QxCUMIxl3S3wpefjksjJbk9PS9g+kqrHMMbfOGWtQ=';
	$header[1] = 'Mundio-Api-PublicKey: UGF5U2FmZQ==';
	$header[2] = 'Content-MD5: PaySafe';
	$header[3] = 'User-Agent: mundiovectone';
	$header[4] = 'Accept: application/json';
	$header[5] = 'Accept-Charset: UTF-8';
	$header[6] = 'Content-Type: application/json';
	$header[7] = 'Host: 192.168.2.102:1123';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
	if ($data) {
		$str = json_encode($data);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $str);
	}
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	
	//echo "<pre>";
	//print_r($header);
	//exit;
    $output = curl_exec($ch);
	curl_close($ch);
    return $output;
}

/**
 * PAYSAFE HEADER API
 * @param type $apiUrl
 * @param type $data
 * @param type $isAuth
 * @return type
 */
function paysafeApiHeaderGet($apiUrl, $isAuth = true) {

    $header[0] = 'Authorization: Basic OGFrMzdnSlVZcTJoUno6MGRpdWQ3NjVqZzk0YnNpODRqZmdqMHczamZoNzgyMmo=';
    $header[2] = 'Content-MD5: paysafe';
    $header[3] = 'User-Agent: vectone-mobile';
    $header[4] = 'Accept: application/json';
    $header[6] = 'Content-Type: application/json';
    $header[7] = 'Host: 192.168.2.102:9810';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, 'geoPlugin PHP Class v1.0');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    if ($isAuth) {
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    }
    $response = curl_exec($ch);
    if ($response === FALSE) {
        $response = htmlspecialchars(curl_errno($ch));
    }
    curl_close($ch);
    return $response;
}

/**
 * PAYSAFE API POST
 * @param type $apiUrl
 * @param type $data
 * @param type $isAuth
 * @return type
 */
function paysafeApiPost($apiUrl, $data = Array(), $isAuth = true) {
    
    if(isset($_SESSION['pasafeApiHeaders']) && $_SESSION['pasafeApiHeaders'] != ""):
        $pasafeApiHeaders = $_SESSION['pasafeApiHeaders'];
    else:
        $pasafeApiHeaders = get_paysafe_api_header();
    endif;

    $header[0] = 'Authorization: mundiovectone ' . $pasafeApiHeaders['postAuthKey'];
    $header[1] = 'Mundio-Api-PublicKey: ' . $pasafeApiHeaders['publicKey'];
    $header[2] = 'Content-MD5: paysafe';
    $header[3] = 'User-Agent: vectone-mobile';
    $header[4] = 'Accept: application/json';
    $header[6] = 'Content-Type: application/json';
    $header[7] = 'Host: 192.168.2.102:9810';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
    if ($data) {
        $str = json_encode($data);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $str);
    }
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

/**
 * Get PaySafe API Header
 */
function get_paysafe_api_header() {
    $response = json_decode(paysafeApiHeaderGet(config_item('get_paysafe_api_header')), TRUE);
    if (isset($response)):
        $pasafeApiHeaders = array(
            "postAuthKey" => isset($response['PostAuthkey']) ? $response['PostAuthkey'] : NULL,
            "publicKey" => isset($response['Publickey']) ? $response['Publickey'] : NULL,
            "getAuthKey" => isset($response['GetAuthkey']) ? $response['GetAuthkey'] : NULL
        );

        $_SESSION['pasafeApiHeaders'] = $pasafeApiHeaders;

        return $pasafeApiHeaders;
    else:
        return 0;
    endif;
}


function apiPasswordPost($apiUrl, $data, $isAuth = true) {

  // set up the curl resource
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen(json_encode($data)),
        'Authorization: Basic OGFrMzdnSlVZcTJoUno6MGRpdWQ3NjVqZzk0YnNpODRqZmdqMHczamZoNzgyMmo=',
        'Host: 192.168.2.102:9038'
    ));
   // execute the request
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}
