<?php

class helper_model extends CI_Model {

    function __construct() {
        parent::__construct();
        if (!isset($_SESSION)) {
            session_start();
        }
        
        $this->load->library('session');
        $this->load->helper('email');
        $this->load->library('email');
        $this->email->set_mailtype("html");
    }

    function getCountryList() {
        $rows = $this->callAPIGetCountry();
        if (!$rows) {
            $rows[91] = 'India';
            $rows[44] = 'UK';
            $rows[94] = 'Sri Lanka';
            $rows[23] = 'Nigeria';
            $rows[63] = 'Philipines';
        }
        return ($rows);
    }

    function getMonthList() {
        $year = ((int) date('Y'));
        $month = ((int) date('m'));
        $date = $year .'-' .$month.'-01';
        for ($i = 0; $i <= 6; $i++) {
            $c = '-' . $i . ' month ';
            $k = date('Y-m-01', strtotime($c, strtotime($date)));
            $rows[$k] = date('F Y', strtotime($c, strtotime($date)));
        }
        return ($rows);
    }

    function getTitleList() {
        $rows = Array();
        $rows['Mr'] = 'Mr';
        $rows['Mrs'] = 'Mrs';
        return ($rows);
    }

    function diePost($var = Array()) {
        echo "<pre>";
        if ($var) {
            print_r($var);
        } else {
            print_r($_POST);
        }
        die();
    }

    function getRingList() {
        for ($i = 1; $i <= 10; $i++) {
            $rows[$i] = $i . ' Rings';
            if ($i < 2) {
                $rows[$i] = $i . ' Ring';
            }
        }
        return ($rows);
    }
	
    function getEnableDisableList() {
		$rows['Yes'] = 'Enable';
		$rows['No'] = 'Disable';
        return ($rows);
	}

    function callAPIGetCountry() {
        if (isset($_SESSION['arrCountryList'])) {
            return $_SESSION['arrCountryList'];
        }
        $url = 'http://192.168.41.23:8912/odata/Countries';
        $response = apiGet($url);
        if (!$response) {
            return Array();
        }
        $response = json_decode($response);
        $response = $response->value;
        $rows = Array();
        foreach ($response as $row) {
            $code = (int) $row->Code;
            $rows[$code] = $row->Name;
        }
        $_SESSION['arrCountryList'] = $rows;
        return ($rows);
    }

    /**
     * Get Country List
     * @return type
     */
    function GetCountryListOperational() {
        $response = json_decode(apiGet(config_item('get_country_list')), TRUE);
        $rows = Array();
        foreach ($response as $row) {
            $code = $row['countrycode'];
            $rows[$code] = ucwords(strtolower($row['countryname']));
        }
        return $rows;
    }

    function getMonthFirstDate($dmy) {
        $date = date('Y-m-01', strtotime($dmy));
        return $date;
    }

    function getMonthEndDate($dmy) {
        $date = date('Y-m-t', strtotime($dmy));
        return $date;
    }

    function getLoggedPhoneNumber() {
        $phoneNumber = '';
        if ($this->session->userdata('userSession')) {
            $userSession = $this->session->userdata('userSession');
            $phoneNumber = $userSession['phoneNumber'];
        }
        return ($phoneNumber);
    }

    function getSubscriberId() {
        $subscriberId = '';
        if ($this->session->userdata('userSession')) {
            $userSession = $this->session->userdata('userSession');
            $subscriberId = $userSession['subscriberId'];
        }
        return ($subscriberId);
    }

    function getSiteCode() {
        $siteCode = '';
        if ($this->session->userdata('userSession')) {
            $userSession = $this->session->userdata('userSession');
            $siteCode = $userSession['siteCode'];
        }
        return ($siteCode);
    }

    function getProductCode() {
        $productCode = '';
        if ($this->session->userdata('userSession')) {
            $userSession = $this->session->userdata('userSession');
            $productCode = $userSession['productCode'];
        }
        return ($productCode);
    }

    function getBrand() {
        $brand = '';
        if ($this->session->userdata('userSession')) {
            $userSession = $this->session->userdata('userSession');
            $brand = $userSession['brand'];
        }
        return ($brand);
    }

    function getCountryCode() {
        $countryCode = '';
        if ($this->session->userdata('userSession')) {
            $userSession = $this->session->userdata('userSession');
            $countryCode = $userSession['countryCode'];
        }
        return ($countryCode);
    }

    function getCurrency() {
        $countryCode = $this->getCountryCode();
		$country = $this->getCurrencyByCode($countryCode);
        return ($country);
    }

    function getCountryName() {
        $countryName = '';
        if ($this->session->userdata('userSession')) {
            $userSession = $this->session->userdata('userSession');
            $countryName = $userSession['countryName'];
        }
        return ($countryName);
    }

    function getCountryNumber() {
        $countryNumber = 0;
        if ($this->session->userdata('userSession')) {
            $userSession = $this->session->userdata('userSession');
            $countryNumber = $userSession['countryNumber'];
        }
        return ($countryNumber);
    }

    function getLanguage() {
        $language = '';
        if ($this->session->userdata('userSession')) {
            $userSession = $this->session->userdata('userSession');
            $language = $userSession['language'];
        }
        return ($language);
    }
	
    function getPassword() {
        $password = '';
        if ($this->session->userdata('userSession')) {
            $userSession = $this->session->userdata('userSession');
            $password = $userSession['password'];
        }
        return ($password);
    }
	
    function getBalance() {
        $balance = 0;
        if ($this->session->userdata('userSession')) {
            $userSession = $this->session->userdata('userSession');
            $balance = $userSession['balance'];
        }
        return ($balance);
    }

    function getMainBalance() {
        $mainbalance = 0;
        if ($this->session->userdata('userSession')) {
            $userSession = $this->session->userdata('userSession');
            $mainbalance = $userSession['mainbalance'];
        }
        return ($mainbalance);
    }
	
    function getCurrencyByCode($code) {
		$currency = 'GBP';
		
		if ($code == 'GB' || $code == 'UK') {
			$currency = 'GBP';
		}
		if ($code == 'AT') {
			$currency = 'EUR';
		}
		if ($code == 'BE') {
			$currency = 'EUR';
		}
		if ($code == 'DK') {
			$currency = 'DKK';
		}
		if ($code == 'FR') {
			$currency = 'EUR';
		}
		if ($code == 'NL') {
			$currency = 'EUR';
		}
		if ($code == 'PL') {
			$currency = 'PLN';
		}
		if ($code == 'PL') {
			$currency = 'PLN';
		}
		if ($code == 'PT') {
			$currency = 'EUR';
		}
		if ($code == 'SE') {
			$currency = 'SEK';
		}
        return ($currency);
    }
    
    /**
     * Get Currency Symbol by Currency
     * @param type $currency
     * @return string
     */
    function getCurrencySignByCurrency($currency) {
        
        $currencySign = '&pound;';
        
        if ($currency == 'GBP') {
            $currencySign = '&pound;';
        }
        if ($currency == 'EUR') {
            $currencySign = '&euro;';
        }
        if ($currency == 'DKK') {
            $currencySign = 'kr';
        }
        if ($currency == 'PLN') {
            $currencySign = 'z&#322;';
        }
        if ($currency == 'SEK') {
            $currencySign = 'kr';
        }
        return $currencySign;
    }
    
     /**
     * 
     * @param type $to
     * @param type $subject
     * @param type $message
     * @return boolean
     */
    function sendEMail($to, $subject, $message, $from) {
        if ($to == '') {
            return false;
        }
        
        $email_object = &$this->email;
        $user = (object) $this->session->userdata('userSession');
        $app = config_item('web_config')[$user->productCode];
        $from_alias = $app['website']." Mobile";

        $email_object->from($from, $from_alias);
        $email_object->to($to);
        $email_object->subject($subject);
        $email_object->message($message);

        $status = $email_object->send();
        $email_object->clear(TRUE);
        return $status;
    }

}

?>