<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
$status = isset($status) ? $status : '';

?>

<div>
    <div class="row panel panel-default">
        <h2 class="sub-header"><?php echo lang('status'); ?></h2>
        
        <div>
            <span><hr/></span>
        </div>
        
        <div class="col-md-12">
				<?php if (strpos($status, 'invalid') > -1) { ?>
                <input type="button" class="form-control col-md-12 btn-danger"  value="<?php echo $status; ?>"/>
				<?php } else { ?>
                <input type="button" class="form-control col-md-12 btn-success"  value="<?php echo $status; ?>"/>
                <?php } ?>
        </div>
        <div>
            <span>&nbsp;</span>
        </div>
    </div>
</div>

<script>

$(document).ready(function() {
});

</script>
