<!-- START CONTENT -->
<section id="main-content" class=" ">
    <section class="wrapper main-wrapper" style=''>

        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
            <div class="page-title">

                <div class="pull-left">
                    <h1 class="title">DASH BOARD</h1>                            </div>

                <div class="pull-right hidden-xs">
                    <ol class="breadcrumb">
                        <li class="active">
                            <strong>DASH BOARD</strong>
                        </li>
                    </ol>
                </div> 
            </div>
        </div>
        <div class="clearfix"></div> 

        <div class="col-lg-12">


            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="r4_counter db_box">
                        <i class='pull-left fa fa-cog fa-spin icon-md icon-rounded icon-primary'></i>
                        <div class="stats">
                            <h4><strong>4</strong></h4>
                            <span>Under Process</span>
                        </div>
                    </div>
                    <div class="tile-counter bg-primary">
                        <ul class="list-unstyled">
                            <a class="text-light pointer" href="#" >Veiw All <i class="fa fa-caret-square-o-right pull-right"></i></a>
                        </ul>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="r4_counter db_box">
                        <i class='pull-left fa fa-check icon-md icon-rounded icon-orange'></i>
                        <div class="stats">
                            <h4><strong>144</strong></h4>
                            <span>Approved</span>
                        </div>
                    </div>
                    <div class="tile-counter bg-orange">
                        <ul class="list-unstyled">
                            <a class="text-light pointer" href="#" >Veiw All <i class="fa fa-caret-square-o-right pull-right"></i></a>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="r4_counter db_box">
                        <i class='pull-left fa fa-ban icon-md icon-rounded icon-purple'></i>
                        <div class="stats">
                            <h4><strong>8</strong></h4>
                            <span>Rejected</span>
                        </div>
                    </div>
                    <div class="tile-counter bg-purple">
                        <ul class="list-unstyled">
                            <a class="text-light pointer" href="#" >Veiw All <i class="fa fa-caret-square-o-right pull-right"></i></a>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="r4_counter db_box">
                        <i class='pull-left fa fa-check-square-o icon-md icon-rounded icon-warning'></i>
                        <div class="stats">
                            <h4><strong>22</strong></h4>
                            <span>Activated</span>
                        </div>
                    </div>
                    <div class="tile-counter bg-warning">
                        <ul class="list-unstyled">
                            <a class="text-light pointer" href="#" >Veiw All <i class="fa fa-caret-square-o-right pull-right"></i></a>
                        </ul>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="r4_counter db_box">
                        <i class='pull-left fa fa-truck icon-md icon-rounded icon-warning'></i>
                        <div class="stats">
                            <h4><strong>1433</strong></h4>
                            <span>Dispatched</span>
                        </div>
                    </div>
                    <div class="tile-counter bg-info">
                        <ul class="list-unstyled">
                            <a class="text-light pointer" href="#" >Veiw All <i class="fa fa-caret-square-o-right pull-right"></i></a>
                        </ul>
                    </div>
                </div>
            </div> <!-- End .row -->	
        </div>
    </section>
</section>