<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php 

$userSession = (object) $this->session->userdata('userSession');

$app = config_item('web_config')[$userSession->productCode];
?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <title>:: ChilliTalk CRM :: </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <?php $this->load->view('layouts/header_script') ?>
    </head>
    <body>
        <input type="hidden" id="site_url" value="<?php echo site_url();?>"  />
        <!-- START CONTAINER -->
        <div>
            <?php $this->load->view($main_content) ?>
        </div>

        <div>
            <?php $this->load->view('layouts/footer') ?>
        </div>
    </body>
    <?php $this->load->view('layouts/footer_script') ?>
</html>
