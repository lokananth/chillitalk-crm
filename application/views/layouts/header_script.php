
<link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.png'); ?>" type="image/x-icon" />    <!-- Favicon -->
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url('assets/images/apple-touch-icon-57-precomposed.png'); ?>" />	<!-- For iPhone -->
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url('assets/images/apple-touch-icon-114-precomposed.png'); ?>" />    <!-- For iPhone 4 Retina display -->
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url('assets/images/apple-touch-icon-72-precomposed.png'); ?>" />   <!-- For iPad -->
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url('assets/images/apple-touch-icon-144-precomposed.png'); ?>" />    <!-- For iPad Retina display -->


<!-- CORE CSS FRAMEWORK - START -->
<link href="<?php echo base_url('assets/plugins/pace/pace-theme-flash.css'); ?>" rel="stylesheet" type="text/css" media="screen"/>
<link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap-theme.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/fonts/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/css/animate.min.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url('assets/plugins/perfect-scrollbar/perfect-scrollbar.css'); ?>" rel="stylesheet" type="text/css"/>
<!-- CORE CSS FRAMEWORK - END -->

<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<link href="<?php echo base_url('assets/plugins/datatables/css/jquery.dataTables.css'); ?>" rel="stylesheet" type="text/css" media="screen"/>
<link href="<?php echo base_url('assets/plugins/datatables/extensions/TableTools/css/dataTables.tableTools.min.css'); ?>" rel="stylesheet" type="text/css" media="screen"/>
<link href="<?php echo base_url('assets/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css'); ?>" rel="stylesheet" type="text/css" media="screen"/>
<link href="<?php echo base_url('assets/plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.css'); ?>" rel="stylesheet" type="text/css" media="screen"/>   
<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


<!-- CORE CSS TEMPLATE - START -->
<link href="<?php echo base_url('assets/css/style.css" rel="stylesheet'); ?>" type="text/css"/>
<link href="<?php echo base_url('assets/css/responsive.css" rel="stylesheet'); ?>" type="text/css"/>
<!-- CORE CSS TEMPLATE - END -->


<!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->

<!-- CORE JS FRAMEWORK - START --> 
<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js'); ?>" type="text/javascript"></script> 
<script src="<?php echo base_url('assets/js/jquery.easing.min.js'); ?>" type="text/javascript"></script> 
<script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script> 
<script src="<?php echo base_url('assets/plugins/pace/pace.min.js'); ?>" type="text/javascript"></script>  
<script src="<?php echo base_url('assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js'); ?>" type="text/javascript"></script> 
<script src="<?php echo base_url('assets/plugins/viewport/viewportchecker.js'); ?>" type="text/javascript"></script>  
<!-- CORE JS FRAMEWORK - END --> 


<!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START --> 
<script src="<?php echo base_url('assets/plugins/icheck/icheck.min.js'); ?>" type="text/javascript"></script><!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 


<!-- CORE TEMPLATE JS - START --> 
<script src="<?php echo base_url('assets/js/scripts.js'); ?>" type="text/javascript"></script> 
<!-- END CORE TEMPLATE JS - END --> 

<!-- Sidebar Graph - START --> 
<script src="<?php echo base_url('assets/plugins/sparkline-chart/jquery.sparkline.min.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/chart-sparkline.js'); ?>" type="text/javascript"></script>
